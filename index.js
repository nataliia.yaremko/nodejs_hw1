const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');

const uploadedFilesLocation = 'uploads';

if (!fs.existsSync(uploadedFilesLocation)) {
    fs.mkdirSync(uploadedFilesLocation);
}

const app = express();

//add other middleware
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan('dev'));
app.use(morgan('common', {
    stream: fs.createWriteStream('./myLogs.log', { flags: 'a' })
}));


app.get('/api/files', async (req, res) => {
    res.send({
        "message": "Success",
        files: fs.readdirSync(uploadedFilesLocation),
    });
});


app.get('/api/files/:filename', async (req, res) => {

    const fileName = req.params.filename;

    const file = fs.readdirSync(uploadedFilesLocation).filter(x => x === fileName)[0];

    if (!file) {
        res.status(400).send({
            message: `No file with '${fileName}' filename found`
        });
        return;
    }

    const serverFilePath = `./${uploadedFilesLocation}/${fileName}`;
    res.send({
        message: "Success",
        filename: fileName,
        content: fs.readFileSync(serverFilePath, { encoding: 'utf-8' }),
        extension: path.extname(fileName).replace('.', ''),
        uploadedDate: fs.statSync(serverFilePath).mtime,
    })
});

// upoad single file
app.post('/api/files', async (req, res) => {
    try {
        //Use the name of the input field (i.e. "avatar") to retrieve the uploaded file
        const uploadedFile = req.body.content;
        const uploadedName = req.body.filename;

        if (!uploadedFile || !uploadedName) {
            res
                .status(400)
                .send({
                    message: `Please specify '${!uploadedFile ? 'content' : 'filename'}' parameter`
                });
                
            return;
        }


        fs.writeFileSync(`./${uploadedFilesLocation}/${uploadedName}`, uploadedFile);

        //send response
        res.send({
            message: "File created successfully"
        });
    } catch (err) {
        console.error(err);
        res.status(500).send(err);
    }
});

//make uploads directory static
app.use(express.static(uploadedFilesLocation));

//start app 
const port = process.env.PORT || 8080;

app.listen(port, () =>
    console.log(`App is listening on port ${port}.`)
);